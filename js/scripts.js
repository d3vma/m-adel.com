new WOW().init();
$(document).ready(function(elements) {
	$('.portfolio-items.owl-carousel').owlCarousel({
		loop: true,
		items: 1,
    pagination: true,
    animateOut: 'fadeOutLeft',
    animateIn: 'fadeInRight',
    autoplay: true,
    lazyLoad : true,
    margin: 20,
    nav:true,
    dots: false,
    navText: ['<img src=\'../img/left-arrow.png\'>','<img src=\'../img/right-arrow.png\'>']
	});

	$('.portfolio-desc-wrapper').css('height',$('.portfolio-img-wrapper').outerHeight());

	$('.quotes-items.owl-carousel').owlCarousel({
		loop: true,
		items: 1,
    pagination: true,
    animateOut: 'fadeOutLeft',
    animateIn: 'fadeInRight',
    autoplay: true,
    margin: 20,
    nav:true,
    dots: false,
    navText: ['<i class="fa fa-long-arrow-left fa-2x" aria-hidden="true"></i>','<i class="fa fa-long-arrow-right fa-2x" aria-hidden="true"></i>']
	});
});